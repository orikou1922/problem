import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

import Sample01 from './components/Sample01.vue'
import Sample02 from './components/Sample02.vue'
import Sample03 from './components/Sample03.vue'
import Sample04 from './components/Sample04.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
      {
        path: '/sample01',
          name: 'sample01',
          component: Sample01
      },
      {
        path: '/sample02',
          name: 'sample02',
          component: Sample02
      },
      {
          path: '/sample03',
          name: 'sample03',
          component: Sample03
      },
      {
          path: '/sample04',
          name: 'sample04',
          component: Sample04
      },


  ]
})
